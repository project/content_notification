<?php

namespace Drupal\content_notification\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\content_notification\ContentNotificationService;
use Drupal\Core\Url;

/**
 * Defines a controller for managing email content notifications.
 */
class ContentNotificationController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * A instance of the content_notification helper services.
   *
   * @var \Drupal\content_notification\ContentNotificationService
   */
  protected $contentNotificationService;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a ContentNotificationController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\content_notification\ContentNotificationService $contentNotificationService
   *   Helper service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    RendererInterface $renderer,
    ContentNotificationService $contentNotificationService,
    LoggerChannelFactoryInterface $loggerFactory,
    MessengerInterface $messenger
    ) {
    $this->renderer = $renderer;
    $this->contentNotificationService = $contentNotificationService;
    $this->loggerFactory = $loggerFactory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('content_notification.common'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Generates list of all email content notification.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function getContentNotificationList() {
    $data = $this->contentNotificationService->getNotifications();
    if ($data) {
      $types = node_type_get_names();
      $rows = [];
      foreach ($data as $value) {
        $enable_disable_link = $value->status ? Link::fromTextAndUrl($this->t('Disable'), Url::fromRoute('content_notification.disable_form', ['notification' => $value->notification_id])) : Link::fromTextAndUrl($this->t('Enable'), Url::fromRoute('content_notification.enable_form', ['notification' => $value->notification_id]));
        $rows[] = [
          $value->notification_id,
          $value->status ? $this->t('Enabled') : $this->t('Disabled'),
          $types[$value->bundle],
          $value->action,
          Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('content_notification.edit_form', ['notification' => $value->notification_id])),
          Link::fromTextAndUrl($this->t('Delete'), Url::fromRoute('content_notification.delete_form', ['notification' => $value->notification_id])),
          $enable_disable_link,
        ];
      }
      // Header of the Notifications list.
      $header = [
        $this->t('Notification Id'),
        $this->t('Status'),
        $this->t('Content Type'),
        $this->t('On Action'),
        $this->t('Edit'),
        $this->t('Delete'),
        $this->t('Enable/Disable'),
      ];
      return [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ];
    }
    else {
      return [
        '#markup' => $this->t('No email content notifcations added yet.'),
      ];
    }
  }

  /**
   * Create email content notification.
   *
   * @return array
   *   The notification add form.
   */
  public function addForm() {
    return $this->formBuilder()->getForm('Drupal\content_notification\Form\ContentNotificationAddForm');
  }

  /**
   * Edit email content notification.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function editForm($notification) {
    return $this->formBuilder()->getForm('Drupal\content_notification\Form\ContentNotificationEditForm', $notification);
  }

  /**
   * Delete email content notification.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function deleteForm($notification) {
    return $this->formBuilder()->getForm('Drupal\content_notification\Form\ContentNotificationDeleteForm', $notification);
  }

  /**
   * Enable email content notification.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function enableForm($notification) {
    $fields = [
      'status' => 1,
    ];
    try {
      $this->contentNotificationService->upDateNotifications($fields, $notification);
      $this->loggerFactory->get('content_notification')->info($this->t('Email content notification has been enabled successfully.'));
      $this->messenger->addMessage('Email content notification has been enabled successfully.');
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('content_notification')->error($e->getMessage());
    }
    return $this->redirect('content_notification.config');
  }

  /**
   * Disable email content notification.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function disableForm($notification) {
    $fields = [
      'status' => 0,
    ];
    try {
      $this->contentNotificationService->upDateNotifications($fields, $notification);
      $this->loggerFactory->get('content_notification')->info($this->t('Email content notification has been disabled successfully.'));
      $this->messenger->addMessage('Email content notification has been disabled successfully.');
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('content_notification')->error($e->getMessage());
    }
    return $this->redirect('content_notification.config');
  }

}
